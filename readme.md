## Flet Draw Things Client

![](assets/ui.png)

Create a Virtual Environment

```
python3 -m venv .venv
```

Activate the virtual environment
```
source .venv/bin/activate
```

Install Flet

```
pip3 install flet
```

Install Pillow to handle images

```
pip3 install Pillow
```

Run the app
```
python3 app.py
```

Open Draw Things and activate the HTTP Server. Make sure it is set to PORT 7860 as this app uses that port.

![](assets/screenshot.png)

If you want to add your own models, update the following code. Make sure the spelling and capitalization is correct.

```
dropdown_menu = Dropdown(
        width=400,
        options=[
            dropdown.Option("Generic (Stable Diffusion v1.5)"),
            dropdown.Option("YourModel"),
            dropdown.Option("YourModel")
        ],
    )
```

You can add other data if you want using these api options. If you send any parameter not on this list, the request is rejected.

```
aesthetic_score
batch_count
batch_size
clip_skip
clip_weight
controls
crop_left
crop_top
decoding_tile_height
decoding_tile_overlap
decoding_tile_width
fps
guidance_scale
guiding_frame_noise
height
hires_fix_height
hires_fix_strength
hires_fix_width
hires_fix
image_guidance
image_prior_steps
loras
mask_blur_outset
mask_blur
model
motion_scale
negative_aesthetic_score
negative_original_height
negative_original_width
negative_prompt
negative_prompt_for_image_prior
num_frames
original_height
original_width
prompt
refiner_model
refiner_start
sampler
seed_mode
seed
sharpness
shift
stage_2_guidance
stage_2_shift
start_frame_guidance
steps
stochastic_sampling_gamma
strength
target_height
target_width
tiled_decoding
width
zero_negative_prompt
```

This is how you'd use a lora:

```
{
  "seed_mode" : "Scale Alike",
  "zero_negative_prompt" : false,
  "loras": [
    {
      "file": "tcd_sdxl_lora_lora_f16.ckpt",
      "weight": 0.5
    }
  ],
  "strength" : 1
}
```