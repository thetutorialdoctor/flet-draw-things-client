from flet import app, Text,TextField,Column,ElevatedButton, Image,ImageFit,GridView,ImageRepeat,Dropdown,dropdown
import requests
import io
import base64
import random
from PIL import Image as IMG
import os
import uuid


def main(page):
    """The main() method contains all of the application code"""

    page.title = "Flet Diffusion"

    # Create a grid for images
    images = GridView(
        expand=1,
        runs_count=5,
        max_extent=150,
        child_aspect_ratio=1.0,
        spacing=5,
        run_spacing=5,
    )

    # Get all images from the assets directory
    photo_images = [_ for _ in os.listdir(os.path.join('./', "assets/")) if _.endswith('png') or _.endswith('jpg') or _.endswith('PNG') or _.endswith('JPG')]

    # Append the images to the images grid as a Flet Image
    for i in photo_images:
        images.controls.append(Image(
            width=400,
            height=400,
            src=i,
            fit=ImageFit.COVER
        ))
    # Update the page to see the changes
    page.update()

    def generate_picture(model,prompt_input,negative_input):
        """This method sends a model, prompt, and negative input to draw things and gets back and loads the returned image into the images grid"""

        url = "http://127.0.0.1:7860"
        print(requests.get(url=f'{url}/docs'))
        payload = {
            "prompt": prompt_input,
            "steps": 15,
            "seed": random.randrange(10000000,100000000),
            "negative_prompt": negative_input,
            "model": model,
            'batch_size': 1
        }
        response = requests.post(url=f'{url}/sdapi/v1/txt2img', json=payload)

        r = response.json()

        for i in r['images']:
            image = IMG.open(io.BytesIO(base64.b64decode(i.split(",",1)[0])))
            filename = str(uuid.uuid4())
            if not os.path.exists('assets'):
                os.makedirs('assets')
            image.save(f'./assets/{filename}.png')
        
        # Append the generated image to the images grid
        img = Image(
            width=400,
            height=400,
            src=f"/{filename}.png",
            fit=ImageFit.COVER
        )
        images.controls.append(
            img
        )

        print('done processing')
        page.update()

    # Create text fields for prompt and negative prompt
    prompt = TextField(label="Prompt", autofocus=True)
    negative_prompt = TextField(label="Negative Prompt")

    greetings = Column()

    # Create a drop-down menu for selecting the Stable Diffusion Model
    dropdown_menu = Dropdown(
        width=400,
        options=[
            dropdown.Option("Generic (Stable Diffusion v1.5)")
        ],
    )

    def btn_click(e):
        """This method calls the generate_picture() method to generate an image and resets the text fields"""
        greetings.controls.append(Text(f"Hello, {prompt.value} {negative_prompt.value}!"))
        
        generate_picture(dropdown_menu.value,prompt.value,negative_prompt.value)
        page.update()

        prompt.value = ""
        negative_prompt.value = ""
        page.update()
        prompt.focus()

    # Add all of the controls to the page
    page.add(
        dropdown_menu,
        prompt,
        negative_prompt,
        ElevatedButton("Generate!", on_click=btn_click),
        greetings,
        images
    )

# Run the app using the main() method as the target
app(target=main)